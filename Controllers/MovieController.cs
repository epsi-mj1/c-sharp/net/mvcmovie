﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace MvcMovie.Controllers.Movies
{
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Models;
    public class MoviesController : Controller
    {
        private static List<Movie> Movies = new List<Movie>
        {
            new Movie
            {
                Id = 1,
                Title = "Un nouvel espoir",
                ReleaseDate = new DateTime(1977, 05, 25),
                Genre = "SF",
                Price = 3.99M
            },
            new Movie
            {
                Id = 2,
                Title = "L'empire contre-attaque",
                ReleaseDate = new DateTime(1980, 05, 21),
                Genre = "SF",
                Price = 4.99M
            },
            new Movie
            {
                Id = 3,
                Title = "Le retour du Jedi",
                ReleaseDate = new DateTime(1983, 05, 25),
                Genre = "SF",
                Price = 5.99M
            }
        };
        // GET: Movies
        public async Task<IActionResult> Index(string movieGenre, string searchString)
        {
            var genres = Movies
                .Select(m => m.Genre)
                .Distinct()
                .OrderBy(g => g);


            var movies = Movies;

            if (!string.IsNullOrEmpty(searchString))
            {
                movies = movies.Where(s => s.Title.Contains(searchString)).ToList();
            }

            if (!string.IsNullOrEmpty(movieGenre))
            {
                movies = movies.Where(m => m.Genre == movieGenre).ToList();
            }

            var movieGenreVM = new MovieGenreViewModel
            {
                Genres = new SelectList(genres),
                Movies = movies
            };

            return View(movieGenreVM);
        }

        // GET: Movies/Details/5
        public ActionResult Details(int id)
        {
            var oneMovie = Movies.Single(movie => movie.Id == id);
            return View(oneMovie);
        }

        // GET: Movies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Movies/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("Id,Title,ReleaseDate,Genre,Price")] Movie movie)
        {
            try
            {
                movie.Id = Movies.Max(m => m.Id) + 1;
                Movies.Add(movie);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Movies/Edit/5
        public ActionResult Edit(int id)
        {
            var oneMovie = Movies.Single(movie => movie.Id == id);
            return View(oneMovie);
        }

        // POST: Movies/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, [Bind("ID,Title,ReleaseDate,Genre,Price")] Movie movie)
        {
            try
            {
                var movieToUpdate = Movies.Single(m => m.Id == id);
                movieToUpdate.Genre = movie.Genre;
                movieToUpdate.Price = movie.Price;
                movieToUpdate.ReleaseDate = movie.ReleaseDate;
                movieToUpdate.Title = movie.Title;

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Movies/Delete/5
        public ActionResult Delete(int id)
        {
            var oneMovie = Movies.Single(movie => movie.Id == id);
            return View(oneMovie);
        }

        // POST: Movies/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, [Bind("Id,Title,ReleaseDate,Genre,Price")] Movie movie)
        {
            try
            {
                var movieToDelete = Movies.Single(m => m.Id == id);
                Movies.Remove(movieToDelete);
                return RedirectToAction(nameof(Index));

            }
            catch
            {
                return View();
            }
        }
    }
}